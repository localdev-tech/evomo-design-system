import React from 'react';
import classNames from 'classnames';
import '../global.css';

export interface ButtonProps
  extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  rounded?: boolean;
  bold?: boolean;
  shadow?: boolean;
  variant?:
    | 'primary'
    | 'secondary'
    | 'outline'
    | 'outline-secondary'
    | 'transparent'
    | 'success'
    | 'danger';
  size?: 'small' | 'medium' | 'large';
  disabled?: boolean;
}

const variants = {
  primary: [
    'bg-evblue-300',
    'text-white',
    'hover:bg-evblue-400',
    'active:bg-evblue-900',
  ], // bg #3C73AA; text #ffffff; bg hover #32608E;
  secondary: [
    'bg-evblue',
    'text-evblue-300',
    'hover:bg-[#DDE5EF]',
    'active:bg-[#BED0E3]',
    'active:text-[#1E3955]',
    'focus:border',
    'focus:border-2',
    'focus:border-[#DDE5EF]',
    'border',
    'border-2',
    'border-transparent',
  ], // bg #F2F9FF; text #3C73AA
  outline: [
    'bg-white',
    'text-evblue-300',
    'border',
    'border-2',
    'border-evblue-300',
    'hover:bg-[#F2F9FF]',
    'active:bg-[#DDE5EF]',
    'active:text-[#1E3955]',
    'active:border-[#1E3955]',
    'focus:border',
    'focus:border-2',
    'focus:border-evblue-300',
  ], // bg #ffffff; text #3C73AA; border #3C73AA
  'outline-secondary': [],
  transparent: [],
  success: [],
  danger: [],
};

const sizes = {
  large: ['p-4', 'text-lg'], // padding 16px 12px; font-size 18px;
  medium: ['px-3', 'py-2.5', 'text-base'], // padding 12px 8px; font-size 16px;
  small: ['px-2', 'py-1', 'text-sm'], // padding 8px 4px; font-size: 14px;
};

const disabledState = ['disabled:bg-evgray', 'disabled:text-evgray-50'];

const Button: React.FC<ButtonProps> = props => {
  const {
    children,
    rounded = true,
    shadow = true,
    variant = 'primary',
    size = 'medium',
    disabled = false,
    bold = true,
  } = props;

  const btnClass = classNames([variants[variant], sizes[size], disabledState], {
    rounded: rounded,
    shadow: shadow,
    'font-bold': bold,
  });

  return (
    <button disabled={disabled} className={btnClass} {...props}>
      {children}
    </button>
  );
};

export default Button;
