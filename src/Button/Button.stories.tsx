import React from 'react';
import { Meta, Story } from '@storybook/react';
import Button, { ButtonProps } from './Button';

const meta: Meta = {
  title: 'Button',
  component: Button,
};
export default meta;

const Component: Story<ButtonProps> = args => <Button {...args}>Default</Button>;

export const Default = Component.bind({});
export const Primary = Component.bind({});
export const Secondary = Component.bind({});
export const Outline = Component.bind({});

Default.args = {};
Primary.args = {
  rounded: true,
  shadow: false,
  bold: true,
  variant: 'primary',
  size: 'medium',
  disabled: false,
};
Secondary.args = {
  rounded: true,
  shadow: false,
  bold: true,
  variant: 'secondary',
  size: 'medium',
  disabled: false,
};
Outline.args = {
  rounded: true,
  shadow: false,
  bold: true,
  variant: 'outline',
  size: 'medium',
  disabled: false,
};
